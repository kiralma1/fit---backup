import collections
import matplotlib.pyplot as plt
from scipy.stats import entropy
import gc

PRINT_PICTURE = 0

class Node:
    pass

class Node:
    def __init__(self, freq: float, symbol: str, left: Node = None, right: Node=None):
        self.freq = freq
        self.symbol = symbol
        self.left = left
        self.right = right
        self.huff = ''

    def __str__(self) -> str:
        res = f'freq: {self.freq}\n'
        res += f'symbol: {self.symbol}\n'
        res += f'left: {self.left.symbol if self.left.symbol else "none"}\n'
        res += f'right: {self.right.symbol if self.right.symbol else "none"}\n'
        res += f'huff: {self.huff}\n'
        res += "-------------"
        return res


def build_huffman(n: Node, buffer: dict, val: str = '',) -> None:
    new_val = val + str(n.huff)
    if n.left:
        build_huffman(n.left, buffer, new_val)
    if n.right:
        build_huffman(n.right, buffer, new_val)
    if not n.left and not n.right:
        buffer[n.symbol] = new_val


def huffman(chars: list) -> list:
    nodes = []

    for char in chars:
        nodes.append(Node(chars[char], char))

    while len(nodes) > 1:
        nodes = sorted(nodes, key=lambda x: x.freq)

        left = nodes[0]
        right = nodes[1]

        left.huff = 0
        right.huff = 1

        newNode = Node(left.freq + right.freq, left.symbol + right.symbol, left, right)

        nodes.remove(left)
        nodes.remove(right)
        nodes.append(newNode)

    res = {}
    build_huffman(nodes[0], res);
    return res


def calculate_probability(freqs: dict, total_count: int) -> dict:
    for key in freqs:
        freqs[key] /= total_count
    return freqs


def read_frequency(path: str) -> dict:
    freq_tmp = collections.defaultdict(float)
    char_count: int = 0

    with open(path, "r") as file:
        for line in file:
            for char in line.strip():
                freq_tmp[char] += 1
                char_count += 1

    return calculate_probability(dict(sorted(freq_tmp.items(), key=lambda x: x[1])), char_count)


def print_freq_code(freqs: dict, code: dict) -> None:
    print(f'Probability of chars')
    print('Char | Probability |     Code     ')
    print('-----+-------------+-------------+')
    for key in freqs:
        print(f'  {key}  |  {freqs[key]:.7f}  |  {code[key]}  ')


def expected_len(frequency: dict, huffman_code: dict) -> int:
    res = 0
    for char in frequency:
        res += frequency[char] * len(huffman_code[char])
    return res


if __name__ == "__main__":
    datasets: list = ["data/015.txt", "data/001.txt"]
    for dataset in datasets:
        print()
        print(f'DATASET: {dataset}')
        print("=================================")
        frequency: dict = read_frequency(dataset)

        if PRINT_PICTURE:
            plt.bar(frequency.keys(), frequency.values(), color="orange")
            plt.show()

        ent = entropy([*frequency.values()], base=2)
        huffman_code = huffman(frequency)
        avg_len = expected_len(frequency, huffman_code)
        print(f"Entropy of chars: {ent:.3f}")
        print(f"Expected code len: {avg_len:.3f}")
        print_freq_code(frequency, huffman_code)

