# Huffman code builder
This short script parses a text file and creates a optimal Huffman code for the particular text encoding

### Usage
- input files are stored in the data directory
- input files to process are defined in the `datasets` variable
- run the builder by running `python3 main.py`
