from numpy.testing import assert_equal
from picture_class import Picture


# ===========================================
# test_images/test_im.png
# ===========================================
# np.array([
#    [1,2,3],
#    [4,5,6],
#    [7,8,9]
# ])
# ===========================================

def test_rotating_clockwise():
    test_picture = Picture('test_images/test_im.png', 'test_path')
    test_picture.rotate_image_clockwise()
    assert_equal([[7, 4, 1], [8, 5, 2], [9, 6, 3]], test_picture.data)


def test_rotating_counterclockwise():
    test_picture = Picture('test_images/test_im.png', 'test_path')
    test_picture.rotate_image_counterclockwise()
    assert_equal([[3, 6, 9], [2, 5, 8], [1, 4, 7]], test_picture.data)


def test_mirroring_image():
    test_picture = Picture('test_images/test_im.png', 'test_path')
    test_picture.mirror_image()
    assert_equal([[3, 2, 1], [6, 5, 4], [9, 8, 7]], test_picture.data)


def test_color_inversion():
    test_picture = Picture('test_images/test_im.png', 'test_path')
    test_picture.inverse_image()
    assert_equal([[254, 253, 252], [251, 250, 249], [248, 247, 246]], test_picture.data)


def test_gray_scale():
    test_picture = Picture('test_images/test_im.png', 'test_path')
    test_picture.to_gray_scale()
    assert_equal([[1, 2, 3], [4, 5, 6], [7, 8, 9]], test_picture.data)


def test_darkening_image():  # 25%
    test_picture = Picture('test_images/test_im.png', 'test_path')
    test_picture.darken_image(25)
    assert_equal([[0.75, 1.5, 2.25], [3., 3.75, 4.5],
                  [5.25, 6., 6.75]], test_picture.data)


def test_lightening_image():  # 25%
    test_picture = Picture('test_images/test_im.png', 'test_path')
    test_picture.lighten_image(25)
    assert_equal([[1.25, 2.5, 3.75], [5., 6.25, 7.5], [
                 8.75, 10., 11.25]], test_picture.data)


def test_sharpen_image():
    test_picture = Picture('test_images/test_im.png', 'test_path')
    test_picture.sharpen_image()
    assert_equal([[0.49609375, 1.8125, 4.06640625], [5.265625, 6.171875, 8.71875], [
                 11.20703125, 12.171875, 14.77734375]], test_picture.data)
