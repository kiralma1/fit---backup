#!/usr/bin/python
# -*- coding: utf-8 -*-
import numpy as np
from PIL import Image


class Picture:

    def __init__(self, input_path, output_path):
        self.path = input_path
        self.out = output_path
        self.image = Image.open(self.path, mode='r')
        self.data = np.array(self.image, dtype=np.float)
        assert self.data.ndim in [2, 3]
        if self.data.ndim == 2:
            self.gray = True
        else:
            self.gray = False

    def rotate_image_counterclockwise(self):  # --rotate_counter
        self.data = np.rot90(self.data, 1)

    def rotate_image_clockwise(self):  # --rotate
        self.data = np.rot90(self.data, 3)

    def mirror_image(self):  # --mirror
        self.data = np.fliplr(self.data)

    def inverse_image(self):  # --inverse
        self.data = 255 - self.data

    def to_gray_scale(self):  # --bw
        if self.data.ndim == 3:
            self.gray = True
            self.data = np.average(
                self.data,
                weights=[
                    0.299,
                    0.587,
                    0.114],
                axis=2).astype(
                np.uint8)

    def darken_image(self, percent):  # --darken(0-100)
        percent = 1.0 - int(percent) / 100
        self.data = self.data * percent

    def lighten_image(self, percent):  # --lighten(0-100)
        percent = 1.0 + int(percent) / 100
        self.data = np.clip(self.data * percent, 0, 255)

    def sharpen_image(self):  # --sharpen
        kernel = -1 / 256 * np.array([[1, 4, 6, 4, 1], [4, 16, 24, 16, 4], [
                                     6, 24, -476, 24, 6], [4, 16, 24, 16, 4], [1, 4, 6, 4, 1]])

        kernel = np.fliplr(np.flipud(kernel))
        out = np.zeros_like(self.data)
        total = int(self.data.shape[1])
        checkpoints = [
            10,
            20,
            30,
            40,
            50,
            60,
            70,
            80,
            90,
        ]

        if self.gray:
            frame = np.zeros((self.data.shape[0] + 4,
                              self.data.shape[1] + 4))
            frame[2:-2, 2:-2] = self.data
            for x in range(2, frame.shape[1] - 2):
                val = int(x / total * 100) % 100 // 10 * 10
                if val in checkpoints:
                    checkpoints.remove(val)
                    print(str(val) + '%')
                for y in range(2, frame.shape[0] - 2):
                    out[y - 2,
                        x - 2] = np.clip((kernel * frame[y - 2:y + 3,
                                                         x - 2:x + 3]).sum(),
                                         0,
                                         255)
        else:
            frame = np.zeros((self.data.shape[0] + 4,
                              self.data.shape[1] + 4,
                              self.data.shape[2]))
            frame[2:-2, 2:-2, ::] = self.data

            for x in range(2, frame.shape[1] - 2):
                val = int(x / total * 100) % 100 // 10 * 10
                if val in checkpoints:
                    checkpoints.remove(val)
                    print(str(val) + '%')
                for y in range(2, frame.shape[0] - 2):
                    for ch in range(self.data.shape[2]):
                        out[y - 2,
                            x - 2,
                            ch] = np.clip((kernel * frame[y - 2:y + 3,
                                                          x - 2:x + 3,
                                                          ch]).sum(),
                                          0,
                                          255)
        self.data = out.copy()

    def save(self):
        if self.gray:
            self.image = Image.fromarray(self.data.astype(np.int8),
                                         mode='L')
            self.image.save(self.out)
        else:
            self.image = Image.fromarray(self.data.astype(np.int8),
                                         mode='RGB')
            self.image.save(self.out)
        print('SAVED TO:', self.out)
