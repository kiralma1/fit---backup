# Graphic_editor

grapihic_editor.py is a simple CLI application, that allows you to run basic image processing alogorithms and edit your pictures using them.
In this repository you can see multiple files.
- graphic_editor.py implenets the graphic editor, its usage is explained later in the README
- tests.py tests the application, by working with small picture, whih can be checked very easily

## Usage

**GETTING STARTED**
```bash
python graphic_editor.py -h
```
or
```bash 
python graphic_editor.py --help 
```

**INPUT FORMAT:**
```bash
python graphic_editor.py ___________________ input_path output_path
```

**INPUT OPTIONS**

- --rotate (rotates image clockwise)          
- --rotate_counter (rotates image counter clockwise)
- --mirror (mirror image)
- --inverse (inverts image colors)
- --sharpen (sharpens image)
- --bw (converts your image to grayscale)
- --lighten LIGHTEN (lightens image by given amount in percent)
- --darken DARKEN (darkens image by given amount in percent)

In input, you can use combination of >= 1 optional arguments (arguments starting with --), resulting in picture edited with multiple algorithms at once.

When using --lighten or --darken, providing 0 <= number <= 100 is neccesary, where the number represents the value in percent, by which you want to lighten/darken your image.

When giving the input/output_path, include also the file name, such as ...dir1/dir2/file.jpg where the picture is stored/will be stored. After the image is edited, you will find it in the file which is represented by given output_path.

**TESTING**

To test the application, just run 
```pytest tests.py```
It is set, to test every feature, the graphic editor offers