import datetime
import time
import argparse
import os
from picture_class import Picture


def check_path(path, type_of_path):
    if type_of_path == 'in':
        if os.path.isfile(path) and os.path.exists(path):
            return 'valid'
        else:
            return 'invalid'
    elif type_of_path == 'out':

        tmp = ''
        for i in range(len(path) - 1, 0, -1):
            tmp = path[0:i]
            if path[i] == '/':
                break
        if os.path.exists(tmp) and os.path.isdir(tmp):
            if os.path.exists(path):
                while 1:
                    x = input(
                        'given output path already exists, do you wish to rewrite existing file? (y/n): ')
                    if x in ['y', 'Y', 'n', 'N']:
                        if x in ['y', 'Y']:
                            return 'valid'
                        else:
                            new_path = input('enter new output path: ')
                            if not os.path.exists(new_path):
                                return new_path
            else:
                return 'valid'
        else:
            return 'invalid'


def main():
    # =================================================
    # ARGUMENT PARSING
    # =================================================
    parser = argparse.ArgumentParser(description='My image editor')
    parser.add_argument(
        '--rotate',
        help='the picture editor will rotate the image 90 degrees clockwise',
        action='store_true')
    parser.add_argument(
        '--rotate_counter',
        help='the picture editor will rotate the image 90 degrees counterclockwise',
        action='store_true')
    parser.add_argument('--mirror',
                        help='the picture editor will mirror given image',
                        action='store_true')
    parser.add_argument(
        '--inverse',
        help='the picture editor will invert colors of given image',
        action='store_true')
    parser.add_argument(
        '--sharpen',
        help='the picture editor will sharpen the edges of given image',
        action='store_true')
    parser.add_argument(
        '--bw',
        help='the picture editor will convert the image to grayscale',
        action='store_true')
    parser.add_argument(
        '--lighten',
        help='the picture editor will lighten the image by given amount in %%')
    parser.add_argument(
        '--darken',
        help='the picture editor will darken the image by given amount in %%')
    parser.add_argument('input_path',
                        help='path to the input image')
    parser.add_argument('output_path',
                        help='path where you want to save your edited image'
                        )
    args = parser.parse_args()

    check = args.rotate or args.rotate_counter or args.mirror \
        or args.inverse or args.sharpen or args.bw or args.lighten \
        or args.darken

    if check is None or check is False:
        print('Using at least one function is mandatory')
        return 1

    # =================================================
    # PATH CHECK
    # =================================================

    if check_path(args.input_path, 'in') == 'invalid':
        print("input path doesn't exist")
        return 1

    outpath = check_path(args.output_path, 'out')
    if outpath == 'invalid':
        print("output direcotry doesn't exist")
        return 1
    elif outpath != 'valid':
        args.output_path = outpath

    # =================================================
    # PICTURE EDITING
    # =================================================

    img = Picture(args.input_path, args.output_path)
    print('INPUT FILE:', args.input_path)
    print('OUTPUT FILE:', args.output_path)
    print('===========================')
    start = time.time()
    if args.inverse:
        print('color inversion: ', end=" ")
        img.inverse_image()
        print('DONE')
    if args.bw:
        print('conversion to gray_scale:', end=" ")
        img.to_gray_scale()
        print('DONE')
    if args.mirror:
        print('image mirroring:', end=" ")
        img.mirror_image()
        print('DONE')
    if args.lighten:
        print('image lightening:', end=" ")
        img.lighten_image(args.lighten)
        print('DONE')
    if args.darken:
        print('image darkening:', end=" ")
        img.darken_image(args.darken)
        print('DONE')
    if args.sharpen:
        print('image sharpening:')
        img.sharpen_image()
        print('image sharpening:  DONE')
    if args.rotate:
        print('image rotation clockwise:', end=" ")
        img.rotate_image_clockwise()
        print('DONE')
    if args.rotate_counter:
        print('image rotation counterclockwise', end=" ")
        img.rotate_image_counterclockwise()
        print('DONE')
    print('===========================')

    # =================================================
    # PICTURE SAVING
    # =================================================

    print('PICTURE EDITED:', datetime.datetime.now())
    img.save()
    print('DURATION:', time.time() - start, 'seconds')


if __name__ == '__main__':
    main()
