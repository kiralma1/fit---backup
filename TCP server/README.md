# TCP server

**In this repository, you can find multiple files**

server.py implements TCP server, which guides robots, throughout their life cycle.

psi-tester-2018-t1-v3_x64 is a binary, implementing the server tester. It creates robots, who connect to the server and ask for guidance


**usgae**

1. run server.py, it will tell you the ip_address and the port number you have to connect to, in order to be able to comunitace with it
2. run ./psi-tester-2018-t1-v3_x64 <port number> <address> [number of test(s)]. You have to add the provided port number and ip-address by server. Optionaly, you can choose which test (out of 1-30) you want to run. By default it runs all the test. Simply just type the number of test, if you want to run it eg. ./psi-tester-2018-t1-v3_x64 <port number> <address> 1 2 3 for test one, two and three