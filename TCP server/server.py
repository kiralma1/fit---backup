# ===========================================================================
# Name        : server.py
# Author      : Marek Király (kiralma1)
# Description : TCP server - BI-PSI
# ===========================================================================
import socket
import os
import threading
import re

# AUTH VALUES
SERVER_KEY = 54621
CLIENT_KEY = 45328
MOD = 65536

# SYSTEM VALUES
BUFFER_SIZE = 1024
MAX_CLIENTS = 10
IP_ADDR = "192.168.56.1"
PORT = 3999

# OTHER
SUFIX = '\a' + '\b'
TIMEOUT = 1
CHARGE_TIMEOUT = 5
CORNERBOYS_TURNS = {0:"right", 1:"left"}

# MESSAGES
CLIENT_USERNAME = "CLIENT_USERNAME"
CLIENT_CONFIRMATION = "CLIENT_CONFIRMATION"
CLIENT_OK = "CLIENT_OK"
CLIENT_FULL_POWER = "FULL_POWER"
CLIENT_MESSAGE = "CLIENT_MESSAGE" 


# ================================================================
class Robot:
	def __init__(self):
		self.buffer = ""
		self.name = 'unknown_name'
		self.x = None
		self.y = None
		self.direction = None
# ================================================================

def sendMessage(c, addr, msg):
    print(f"S: {addr[0]}:{addr[1]} - {msg}{SUFIX}")
    c.sendall(bytes(str(msg) + SUFIX, "utf-8"))


def recvMessage(client, addr, max_len, robot):
	if SUFIX not in robot.buffer:
		msg = ""
		tmp = ""
		tmp_dec = ""
		curr_len = len(robot.buffer)

		while SUFIX not in msg:
			if curr_len >= max_len:
				return False
			tmp = client.recv(BUFFER_SIZE)
			tmp_dec = tmp.decode()
			msg += tmp_dec
			curr_len += len(tmp_dec)
			
		robot.buffer += msg
	return True


def verifyMessage(expected_msg, robot):
	if robot.buffer.startswith("RECHARGING"+SUFIX):
		robot.buffer = robot.buffer.replace("RECHARGING"+SUFIX, "", 1)
		return "RECHARGING"

	if expected_msg == CLIENT_USERNAME:
		expr = re.findall(r'^.{0,10}[\x07][\x08]', robot.buffer) # MAX_LEN == 12
		if len(expr) != 0:
			robot.buffer = robot.buffer.replace(expr[0], "", 1)
			return expr[0][:-2]

	elif expected_msg == CLIENT_CONFIRMATION:
		expr = re.findall(r'^\d{1,5}[\x07][\x08]', robot.buffer) # MAX_LEN == 7
		if len(expr) != 0:
			robot.buffer = robot.buffer.replace(expr[0], "", 1)
			return expr[0][:-2]
			
	elif expected_msg == CLIENT_OK:
		expr = re.findall(r'^OK -?\d* -?\d*[\x07][\x08]', robot.buffer) # MAX_LEN == 12
		if len(expr) != 0:
			if len(expr[0]) <= 12:
				robot.buffer = robot.buffer.replace(expr[0], "", 1)
				return expr[0][:-2]

	elif expected_msg == CLIENT_FULL_POWER:
		expr = re.findall(r'^FULL POWER[\x07][\x08]', robot.buffer) # MAX_LEN == 12
		if len(expr) != 0:
			robot.buffer = robot.buffer.replace(expr[0], "", 1)
			return expr[0][:-2]
			
	elif expected_msg == CLIENT_MESSAGE:
		expr = re.findall(r'^.{0,100}[\x07][\x08]', robot.buffer) # MAX_LEN == 100
		if len(expr) != 0:
			robot.buffer = robot.buffer.replace(expr[0], "", 1)
			return expr[0][:-2]
	return None


def recvLoop(c, addr, l, msg_to_check, robot):
	while True:
		if not recvMessage(c, addr, l, robot):
			sendMessage(c, addr, "301 SYNTAX ERROR")
			return False, 'SYNTAX_ERR'
		msg = verifyMessage(msg_to_check, robot)
		print(f"R: {addr[0]}:{addr[1]} - {msg}")
		if msg == "RECHARGING":
				c.settimeout(5)
				if not recvMessage(c, addr, 12, robot):
					sendMessage(c, addr, "301 SYNTAX ERROR")
					return False, 'SYNTAX_ERR'
				msg = verifyMessage(CLIENT_FULL_POWER, robot)
				print(f"R: {addr[0]}:{addr[1]} - {msg}") 
				if msg is None:
					sendMessage(c, addr,"302 LOGIC ERROR")
					return False, "RECHARGING_ERR"
				c.settimeout(1)
		elif msg is None:
			sendMessage(c, addr, "301 SYNTAX ERROR")
			return False, 'VERIFY_ERR'
		else:
			return True, msg
			

def countHash(name):
    tmp = 0
    for i in name:
        tmp += ord(i)
    return (tmp * 1000) % MOD


def Authentificate(c, addr, robot):
	tmp = recvLoop(c, addr, 12, CLIENT_USERNAME, robot)
	if not tmp[0]:
		return tmp
	else:
		name = tmp[1]
	# NAME ACQUIRED====================================================================
	nameHash = countHash(name)
	sendMessage(c, addr, (nameHash + SERVER_KEY) % MOD)

	tmp = recvLoop(c, addr, 7, CLIENT_CONFIRMATION, robot)
	if not tmp[0]:
		return tmp
	else:
		check = tmp[1]
	# CHECK ACQUIRED====================================================================	
	if check != str((nameHash + CLIENT_KEY) % MOD):
		sendMessage(c, addr, "300 LOGIN FAILED")
		return False, name
	else:
		sendMessage(c, addr, "200 OK")
		return True, name


def endConnection(c, addr, name):
    print(f"S: {addr[0]}:{addr[1]} - CONNECTION ENDED: {name}")
    c.close()


def investigateDirection(coords):  # 0=up, 1=right, 2=down, 3=left
	x1, y1, x2, y2 = coords[0], coords[1], coords[2], coords[3] 
	if x1 > x2:
		return 3
	elif x1 < x2:
		return 1
	elif y1 < y2:
		return 0
	elif y1 > y2:
		return 2


def move(c, addr, robot):
	x = robot.x
	y = robot.y
	while (x == robot.x and y == robot.y):
		sendMessage(c, addr, "102 MOVE")
		tmp = recvLoop(c, addr, 12, CLIENT_OK, robot)
		if not tmp[0]:
			return tmp
		else:
			tmp = tmp[1][2:].split()
			x, y = int(tmp[0]), int(tmp[1])
	return True, int(x), int(y)


def investigateCoords(c, addr, robot):
	coords = []

	for _ in range(2):
		tmp = move(c, addr, robot)
		if not tmp[0]:
			return False
		else:
			coords.append(tmp[1])
			coords.append(tmp[2])
			robot.x, robot.y = tmp[1], tmp[2]
	robot.direction = int(investigateDirection(coords))
	return True


def turn(c, addr, robot, side):
	if side == "left":
		robot.direction = (robot.direction - 1) % 4
		sendMessage(c, addr, "103 TURN LEFT")
	elif side == "right":
		robot.direction = (robot.direction + 1) % 4
		sendMessage(c, addr, "104 TURN RIGHT")
	
	tmp = recvLoop(c, addr, 12, CLIENT_OK, robot)
	if not tmp[0]:
		return False
	else:
		return True


def robotJourney(c, addr, robot):
	if robot.x <= -2:
		if robot.direction == 2 or robot.direction == 3:
			for _ in range(robot.direction - 1):
				if not turn(c, addr, robot, "left"):
					return False
		elif robot.direction == 0:
			if not turn(c, addr, robot, "right"):
				return False

	elif robot.x >= 2:
		if robot.direction == 0 or robot.direction == 1:
			for _ in range(robot.direction + 1):
				if not turn(c, addr, robot, "left"):
					return False
		elif robot.direction == 2:
			if not turn(c, addr, robot, "right"):
				return False

	while robot.x < -2 or robot.x > 2:
		tmp = move(c, addr, robot)
		if not tmp[0]:
			return False
		else:
			robot.x, robot.y = tmp[1], tmp[2]

	if robot.y >= 2:
		if robot.direction == 0 or robot.direction == 1:
			for _ in range(2 - robot.direction):
				if not turn(c, addr, robot, "right"):
					return False
		elif robot.direction == 3:
			if not turn(c, addr, robot, "left"):
				return False

	elif robot.y <= -2:
		if robot.direction == 1 or robot.direction == 2:
			for _ in range(robot.direction):
				if not turn(c, addr, robot, "left"):
					return False
		elif robot.direction == 3:
			if not turn(c, addr, robot, "right"):
				return False

	while robot.y < -2 or robot.y > 2:
		tmp = move(c, addr, robot)
		if not tmp[0]:
			return False
		else:
			robot.x, robot.y = tmp[1], tmp[2]

	return True


def pickupMsg(c, addr, robot):
	sendMessage(c, addr, "105 GET MESSAGE")
	
	tmp = recvLoop(c, addr, 100, CLIENT_MESSAGE, robot)
	if not tmp[0]:
		return 0
	else:
		if len(tmp[1]):
			return 2
		else:
			return 1


def cornerBoys(c, addr, robot):
	coords = (robot.x, robot.y)
	if (coords == (-2, 2) and robot.direction == 2) or 
	    (coords == (2, 2) and robot.direction == 3) or 
	    (coords == (2, -2) and robot.direction == 0) or 
	    (coords == (-2, -2) and robot.direction == 1):
		if not turn(c, addr, robot, 'left'):
			return False
	
	if (coords == (-2, 2) and robot.direction == 0) or 
	    (coords == (2, 2) and robot.direction == 1) or 
	    (coords == (2, -2) and robot.direction == 2) or 
	    (coords == (-2, -2) and robot.direction == 3):
		if not turn(c, addr, robot, 'right'):
			return False

	if (coords == (-2, 2) and robot.direction == 3) or 
	    (coords == (2, 2) and robot.direction == 0) or 
        (coords == (2, -2) and robot.direction == 1) or 
        (coords == (-2, -2) and robot.direction == 2):
		for _ in range(2):
			if not turn(c, addr, robot, 'left'):
				return False

	for i in range(4):
		side = CORNERBOYS_TURNS[i%2]

		for _ in range(4):
			tmp = pickupMsg(c, addr, robot)
			if tmp == 0: #syntax error
				return False
			elif tmp == 2: #msg found
				return True
			# else msg not found => continue
			tmp = move(c, addr, robot)
			if not tmp[0]:
				return False
			else:
				robot.x, robot.y = tmp[1], tmp[2]

		# pickup====================================
		tmp = pickupMsg(c, addr, robot)
		if tmp == 0: #syntax error
			return False
		elif tmp == 2: #msg found
			return True
		# turn=====================================
		if not turn(c, addr, robot, side):
			return False
		# move=====================================
		tmp = move(c, addr, robot)
		if not tmp[0]:
			return False
		else:
			robot.x, robot.y = tmp[1], tmp[2]
		# pickup====================================
		tmp = pickupMsg(c, addr, robot)
		if tmp == 0: #syntax error
			return False
		elif tmp == 2: #msg found
			return True
		# turn=====================================
		if not turn(c, addr, robot, side):
			return False

	for _ in range(4):
		tmp = pickupMsg(c, addr, robot)
		if tmp == 0: #syntax error
			return False
		elif tmp == 2: #msg found
			return True
		# else msg not found => continue
		tmp = move(c, addr, robot)
		if not tmp[0]:
			return False
		else:
			robot.x, robot.y = tmp[1], tmp[2]

	tmp = pickupMsg(c, addr, robot)
	if tmp == 0: #syntax error
		return False
	elif tmp == 2: #msg found
		return True
	else:
		print('WTF?? WHERE IS THE MESSAGE?')
		return False
		

def findCorner(c, addr, robot):
	if not turn(c, addr, robot, "left"):
		return False
	
	coords = (robot.x, robot.y)
	while coords != (-2, 2) and coords != (2, 2) and coords != (2, -2) and coords != (-2, -2):
		tmp = move(c, addr, robot)
		if not tmp[0]:
			return False
		else:
			robot.x, robot.y = tmp[1], tmp[2]
			coords = (robot.x, robot.y)

	if not turn(c, addr, robot, "right"):
		return False

	return True


def getToBorder(c, addr, robot):
	while robot.x != -2 or robot.x != 2 or robot.y != -2 or robot.y != 2:
		tmp = move(c, addr, robot)
		if not tmp[0]:
			return False
		else:
			robot.x, robot.y = tmp[1], tmp[2]

	for _ in range(2):
		if not turn(c, addr, robot, "right"):
			return False


def serverLogout(c, addr, robot):
	sendMessage(c, addr, "106 LOGOUT")
	endConnection(c, addr, robot.name)


def newClientFlowFunction(c, addr):
	try:
		# ATHENTIFICATION====================================================
		print(f"S: {addr[0]}:{addr[1]} - NEW CONNECTION")
		robot = Robot()
		c.settimeout(1)
		auth = Authentificate(c, addr, robot)  # auth = (T/F, client_name)

		if not auth[0]:
			endConnection(c, addr, auth[1])
			return

		# ROBOT CONNECTION AND DEFINITION====================================
		print(f"S: {addr[0]}:{addr[1]} - LOGGED IN")
		robot.name = auth[1]

		if not investigateCoords(c, addr, robot):
			endConnection(c, addr, robot.name)
			return False

		if not robotJourney(c, addr, robot):
			endConnection(c, addr, robot.name)
			return False
			
		# GETTING TO CORRECT CORNER AND PICKING UP MESSAGE===================
		coords = (robot.x, robot.y)
		if coords == (-2,2) or coords == (2,2) or coords == (2,-2) or coords == (-2,-2): #if robot in corner
			if not cornerBoys(c, addr, robot):
				endConnection(c, addr, robot.name)
				return False

		elif robot.x == 2 or robot.x == -2 or robot.y == 2 or robot.y == -2: #if robot on border
			if not findCorner(c, addr, robot): 
				endConnection(c, addr, robot.name)
				return False
			if not cornerBoys(c, addr, robot): 
				endConnection(c, addr, robot.name)
				return False

		else: #if robot inside square
			if not getToBorder(c, addr, robot):
				endConnection(c, addr, robot.name)
				return False 
			if not findCorner(c, addr, robot):
				endConnection(c, addr, robot.name)
				return False
			if not cornerBoys(c, addr, robot):
				endConnection(c, addr, robot.name)
				return False
		
		# ENDING CONNECTION==================================================
		serverLogout(c, addr, robot)
		return True	
		
	
	except socket.timeout:
		print('CAUGHT TIMEOUT')
		endConnection(c, addr, "empty login")
		return


def main():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((IP_ADDR, PORT))
    s.listen(MAX_CLIENTS)
    print(f"Server {IP_ADDR}:{PORT}, is up and running !")
    print("-----------------------------------------------")

    while True:
        try:
            c, addr = s.accept()
            threading._start_new_thread(newClientFlowFunction, (c, addr))
        except KeyboardInterrupt as e:
            print(f"The server is shutting down")
            s.close()
            return 1
        except Exception as e:
            print(f"Well this went wrong: {e}")
            s.close()
            return 1


if __name__ == "__main__":
    main()

