from enum import Enum

# ENUM CLASS ULAHCUJUCA ROZOZNANIE TYPU SOLVERU
class STYPE(Enum):
    JACOBI = "jacobi"
    GAUSS_SEIDEL = "gauss_seidel"