from Stype import *
from Conf import *

import numpy as np
np.seterr(all = "raise")
np.seterr(divide='raise')

#TRIEDA SLR_Solver OBSAHUJE METODY POTREBNE K RIESENIU SLR OBOMA POSTUPMI
class SLR_solver:
    def __init__(self, n: int=20):
        self.n = n

    # POMOCNE METODY NA VYTVORENE MATIC -----------------------------------------------------------
    def createMatrixA(self):
        self.A = np.zeros((self.n, self.n), dtype=DTYPE)
        np.fill_diagonal(self.A, self.param)
        np.fill_diagonal(self.A[1:], -1)
        np.fill_diagonal(self.A[:,1:], -1)

    def createMatrixB(self):
        self.B = np.full((self.n, 1), self.param - 2, dtype=DTYPE)
        self.B[0] += 1
        self.B[-1] += 1

    def createMatrixL(self, k_param : int = -1):
        self.L = np.tril(self.A, k=k_param)

    def createMatrixU(self):
        self.U = np.triu(self.A, k=1)

    def createMatrixX(self):
        self.Xs = np.zeros((ITER_MAX + 1, self.n, 1), dtype=DTYPE)

    def JcreateMatrixD(self):
        self.D = np.diag(np.full(self.n,self.param, dtype=DTYPE))
        self.D_inverse = np.linalg.inv(self.D)

    def GScreateMatrixD(self, omega):
        self.D = np.diag(np.diag(self.A)) + self. L
        self.D_inverse = np.linalg.inv(self.D)
    
    # ---------------------------------------------------------------------------------------------

    # METODA NA ZISKANIE AKTUALNE VYUŽIVANYCH MATIC
    def presentMatrices(self):
        print(f'\nA({self.A.shape}):\n------------\n', self.A)
        print(f'\nB({self.B.shape}):\n------------\n', self.B)
        print(f'\nD({self.D.shape}):\n------------\n', self.D)
        print(f'\nL({self.L.shape}):\n------------\n', self.L)
        print(f'\nU({self.U.shape}):\n------------\n', self.U)

    # METODA NA VYPIS VYSEDLKU VYPOČTU - VOLÁ SA AUTOMATICKY PO JEHO ÚSPEŠNOM DOKONČENÍ
    def printSolution(self):
        print(f'Solution found in {self.k} itreations')
        print(f'x: {np.transpose(self.Xs[self.k])}')

    # KONTROLA SPLNENIEA PODMIENKY PRESNOSTI
    def checkX(self):
        return np.linalg.norm((self.A @ self.Xs[self.k]) - self.B)  / np.linalg.norm(self.B) < LIMIT
    
    # METODA VYUŽÍVA POMOCNÉ METÓDY createMatrix<type> A INICIALIZUJE TAK MATICE PRED SAMOTNÝM VÝPOČTOM
    def prepMatrices(self, omega: DTYPE=1):
      
        self.createMatrixA()
        self.createMatrixB()
        self.createMatrixL()
        self.createMatrixU()
        self.createMatrixX()
        self.LU = self.L + self.U
        
        if self.type == JACOBI:
            self.JcreateMatrixD()
        else:
            self.GScreateMatrixD(omega)

    # METODA NA ZISTOVANIE SPEKTRALNEHO RADIUSU ZA UCELOM OVERENIA KONVERGENICE
    def spectralRadius(self):
        d = np.linalg.inv(np.diag(np.diag(self.A)))
        return max(abs(np.linalg.eig(d @ self.LU)[0]))

    # METÓDA NA VÝPOČET SLR JACOBIHO METÓDOU
    def calculateMatrix(self, param: DTYPE, type: STYPE=STYPE.JACOBI, omega : int = 1):
        self.type = type
        self.param = param
        
        self.prepMatrices(omega)
        print(f'Solving SLR for gama = {self.param}...\nmethod: {self.type.name}\n...') 

        if self.spectralRadius() >= 1:
            print("ERR: method doesn't converge")
            return
        self.k = 0

        for self.k in range(ITER_MAX):
            try:
                if self.checkX():
                    self.printSolution()
                    return
                else:
                        self.Xs[self.k + 1] = \
                            self.D_inverse @ (self.B - (self.LU @ self.Xs[self.k])) \
                                if self.type == JACOBI else \
                            self.D_inverse @ (((self.D - self.A) @ self.Xs[self.k]) + self.B)
            except ValueError: # VYVOJOVY CASE - NARAZAL SOM NA PROBLEM S TAVRMI MATIC
                exit("shapeERR")
            except FloatingPointError: # ODCHYTI PRIPADU, KEDY METODA NEKONVERGUJE
                print(f'Stopping calculation: method doesn\'t converge - spectral radius check failed\nErr occured after {self.k} iterations')
                return
            self.k += 1

        # POKIAL METODA NEOBJAVI DOSTATOCNE DOBRY VYSLEDOK V <ITER_MAX> ITERACIACH, \
        # VYPOCET SA ZASTAVI A PROGRAM VYHALSI NEUSPECH
        print(f'Solution not found in {self.k} iterations')
