from Solver import *
from Stype import *
from Conf import *

if __name__ == "__main__":
    solver = SLR_solver(n=20)

    # VYPOCET JACOBIHO METODOU------------------------
    for gama in VALS:
        solver.calculateMatrix(gama)
        print(SEP)

    print("\n"*3+SEP)
    
    # VYPOCET GAUSS-SEIDELOVOU METODOU----------------
    for gama in VALS:
        solver.calculateMatrix(gama, type=GAUSS_SEIDEL)
        print(SEP)