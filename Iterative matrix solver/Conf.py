import numpy as np
from Stype import *

# NASTAVENIE KONSTANTNYCH HODNOT
DTYPE = np.double
LIMIT = 10**-6
ITER_MAX = 10000
JACOBI = STYPE.JACOBI
GAUSS_SEIDEL = STYPE.GAUSS_SEIDEL
SEP = "====================================="
VALS = [5, 2, 0.5]