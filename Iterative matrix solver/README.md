# Iterative matrix solver

Iterative matrix solver offers 2 methods, Jacobi and Gauss-Seidel, to solve a system of linear equations. 
- First, create an instance of the SLR_Solver() object. Object requires the *n* parameter, which specifies the size of the initial matrix (default == 20). 
- Then use the calculateMatrix(gama, type) method, to solve the eqations.
          - gama: values 5, 2 and 0.5 were set in the original asignment.
          - type: defines used method to solve the system. Values can be set to JACOBI (default) or GAUSS_SEIDEL



#### Usage of the solver is displayed bellow. 
![](./usage.png)

(Default values and matrices were set in the original asignment)