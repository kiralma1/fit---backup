# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter


class WebshopCrawlerPipeline:

    def __init__(self):
        self.items = []

    def process_item(self, item, spider):
        self.items.append(item)
        return item

    def close_spider(self, spider):
        MAX = 80
        with open("data.json", mode="w") as out:
            print("[", file=out)
            tmp = list(sorted(self.items, key=lambda x: (x['rating'], x['price']), reverse=True))[:MAX]
            print(*tmp, file=out, sep=",\n")
            print("]", file=out)

