import scrapy


class AlzaSpider(scrapy.Spider):
    name = "alza_spider"
    start_urls = ["https://www.alza.cz/gaming/playstation-4-hry/18854417.htm"]

    def parse(self, response):
        for game_div in response.css(".browsingitem"):
            yield {
                'name': self.get_game_name(game_div),
                'price': self.get_game_price(game_div),
                'rating': self.get_game_rating(game_div),
                'picture': self.get_game_image(game_div)
            }

        next_page = response.urljoin(response.css(".next::attr(href)").get())
        if next_page is not None:
            yield response.follow(next_page, callback=self.parse)

    @staticmethod
    def get_game_name(game: scrapy.Selector) -> str:
        return game.css(".browsinglink::text").extract()[-1].replace(" - PS4", "").strip()

    @staticmethod
    def get_game_price(game: scrapy.Selector) -> int:
        try:
            return int(game.css(".c2::text").extract()[0].replace(",-", "").replace(u'\xa0', u''))
        except Exception as e:
            return 999999

    @staticmethod
    def get_game_rating(game: scrapy.Selector) -> float:
        try:
            return float(game.css(".star-rating-wrapper::attr(title)").extract()[0].replace("Hodnocení ", "").replace("/5", "").replace(",", "."))
        except Exception:
            return 0.0

    @staticmethod
    def get_game_image(game: scrapy.Selector) -> str:
        return game.css("img::attr(data-src)").extract()[0]

