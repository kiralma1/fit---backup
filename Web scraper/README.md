# Web scraper
This short script uses python scrapy module, to scrape information of the www.alza.cz PS4 games section. 

### Usage
- run the script by typing `python3 main.py` in your console.
- output data sotred and filtered will appear in the data.json file in the root of the directory.
