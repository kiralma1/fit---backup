
# =========REQUIREMENTS================
# python -m pip install validate_email
# python -m pip install py3DNS
# =====================================

import json
from os import error
from typing import DefaultDict
from urllib.request import urlopen
from collections import defaultdict
from validate_email import validate_email
from threading import Thread, Lock

EMAILS_DB = "./data/emails.txt"
VALID_EMAILS = "./out/valids.txt"
INVALID_EMAILS = "./out/errors.txt"
LOG_FILE = "./out/log.txt"
LOAD_BAR_LEN = 100
CHECK_SMTP = True
NUM_OF_THREADS = 5

def verifyEmail(emails, errors, valids, log):
    lock = Lock()
    lock_e = Lock()
    lock_v = Lock()
    lock_p = Lock()
    lock_p2 = Lock()
    global i

    while(len(emails)):
        lock.acquire()
        email = emails.pop()
        i = i + 1
        lock.release()

        try:
            if validate_email(email, verify=CHECK_SMTP):
                lock_p.acquire()
                print(f'{email} : OK')
                print(f'{email} : OK', file=log)
                lock_p.release()
                
                lock_v.acquire()
                valids.add(email)
                lock_v.release()
            else:
                lock_p.acquire()
                print(f'{email} : FAIL (information stored)')
                print(f'{email} : FAIL (information stored)', file=log)
                lock_p.release()

                lock_e.acquire()
                errors[email] += 1 
                lock_e.release()
        except KeyboardInterrupt:
            exit()
        except:
            lock_p.acquire()
            print(f'{email} : EMPTY OR TOO LONG (information stored)')
            print(f'{email} : EMPTY OR TOO LONG (information stored)', file=log)
            lock_p.release()

            lock_e.acquire()
            errors[email] += 1 
            lock_e.release()
        
        lock_p2.acquire()
        progress = (i)/l_emails
        print(f'==( {round(progress*100, 2)}% )' + '='*int((LOAD_BAR_LEN*progress)) + '_'*(LOAD_BAR_LEN - int((LOAD_BAR_LEN*progress))))
        print(f'==( {round(progress*100, 2)}% )' + '='*int((LOAD_BAR_LEN*progress)) + '_'*(LOAD_BAR_LEN - int((LOAD_BAR_LEN*progress))), file=log)
        lock_p2.release()

if __name__ == "__main__":
    errors = defaultdict(lambda : 0)
    valids = set()
    log = open(LOG_FILE, 'w')

    f = open(EMAILS_DB, 'r')
    emails = f.read().strip().split('\n')
    f.close()

    l_emails = len(emails)
    global i
    i = 0
    threads = []

    for i in range(NUM_OF_THREADS):
        tmp = Thread(target=verifyEmail, args=(emails, errors, valids, log))
        tmp.start()
        threads.append(tmp)
    
    for thread in threads:
        thread.join()

    f = open(INVALID_EMAILS, 'w')
    for record, num in errors.items():
        print(f'{record} - {num}', file=f)
    f.close()

    f = open(VALID_EMAILS, 'w')
    print(*valids, sep="\n", end="", file=f)
    f.close()

    overall = len(valids) + len(errors)
    for thread in threads:
        thread.join()

    tmp_str = "\n\n\n======================\n"
    print(tmp_str)
    print(tmp_str, file=log)

    tmp_str = "ALL EMAILS VERIFIED AND DUPLICITIES REMOVED"
    print(tmp_str)
    print(tmp_str, file=log)

    tmp_str = f'VALID EMAILS: {len(valids)} ({round(len(valids)/overall*100, 2)}%) --> {VALID_EMAILS}'
    print(tmp_str)
    print(tmp_str, file=log)

    tmp_str = f'INVALID EMAILS: {len(errors.items())} ({round(len(errors.items())/overall*100, 2)}%) --> {INVALID_EMAILS}'
    print(tmp_str)
    print(tmp_str, file=log)

    tmp_str = "\n======================"
    print(tmp_str)
    print(tmp_str, file=log)
    
    log.close()