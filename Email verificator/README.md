# Email verification

Email verificator uses validate_email library to run through a list of emails and validates their syntax and existence, while also utilizes the power of threading to make parallel verification of multiple emails possible in order to speed up the computation time. All data necessary to output are stored in the ./out directory, where in valids.txt valid email addresses are stored and in errors.txt invalid emails with thouer count in the original db are stored.

- Insert your email adddresses into ./data/email.txt separated by newline
- Run the main.py file
- Analyze the output in the ./out directory