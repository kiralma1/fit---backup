# INSPIRED BY https://moodle-vyuka.cvut.cz/mod/page/view.php?id=153040 and https://moodle-vyuka.cvut.cz/mod/page/view.php?id=153039
import glob
import time



instance_recursion_count = 0
INSTANCE_LENGTH = 500
INSTANCE_LENGTH_ZKW = 2000
MAX_INT = float("inf")
PRINT = 0
RESULTS_TIME = []

class Instance:
    def __init__(self, id, n, M):
        self.id = int(id)
        self.n = int(n)  # number of items
        self.M = int(M)  # backpack capacity
        self.prices = []  # items - prices
        self.weights = []  # items - weight
        self.table = []
        self.max_price = -1
        self.max_nodes = ['0' for x in range(self.n)]

        self.total_prices = 0

    def iprint(self):
        print("=================================================")
        print("ID:", self.id)
        print("n:", self.n)
        print("M:", self.M)
        print("weights: {"+str(self.weights)[1:-1]+"}")
        print("prices: {"+str(self.prices)[1:-1]+"}")
        print()
        print("max:", self.max_price)
        print(f'used items: {self.max_nodes}')
        print("--------------------------------")

    def table_print(self):
        for i, line in enumerate(reversed(self.table)):
            print(f'{"{:3}".format(len(self.table) - 1 - i)}|', end="")
            for cell in line:
                print(" ", end="")
                print(cell, end=" ")
            print()

        for i in range(self.n + 2):
            print(f'---', end="-")
        print()
        print("  #|", end=" ")
        for i in range(self.n + 1):
            print(f' {i} ', end="|")
        print()

    def buildSolution(self):
        for i, line in enumerate(reversed(self.table)):
            if line[-1] <= self.M:
                self.max_price = self.total_prices - i
                break

        tmp_max = self.max_price
        for i in range(self.n, 0, -1):
            if tmp_max < 0:
                tmp_max = 0

            if self.table[tmp_max][i] != self.table[tmp_max][i - 1]:
                tmp_max -= self.prices[i - 1]
                self.max_nodes[i - 1] = '1'

    def prepTable(self):
        self.total_prices = sum(self.prices)
        self.table = [[0 for _ in range(self.n + 1)] for row in range(self.total_prices + 1)]
        
        for line in self.table:
            line[0] = MAX_INT
        self.table[0][0] = 0

    def solveKnapsack_DP(self):      
        for row in range(1, self.n+1):
            for col in range(0, self.total_prices + 1):
                if col >= self.prices[row - 1]:
                    self.table[col][row] = min(
                        self.table[col][row - 1],
                        self.table[col - self.prices[row - 1]][row - 1] + self.weights[row - 1]
                    )
                else:
                    self.table[col][row] = self.table[col][row - 1]

        return self.buildSolution()
        

def getFilesInFolder(folder):
    return glob.glob('./data/'+folder+'/*.txt')


def parseFile(file, path):
    file_split = file.split(" ")
    ins = Instance(file_split[0], file_split[1],
                   file_split[2])
    file_split = file_split[3:]
    for i in range(0, len(file_split), 2):
        ins.weights.append(int(file_split[i]))
        ins.prices.append(int(file_split[i+1]))  

    return ins


def getAnswers(path):
    p = path.split("/")
    p[-1] = p[-1].replace("inst", "sol")
    path = '/'.join(p)
    file = open(path, "r")
    print(f'GETTING ANSWERS FROM: {path}')

    res = [[] for _ in range(INSTANCE_LENGTH)] if "ZKW" not in path else [[] for _ in range(INSTANCE_LENGTH_ZKW)]
    for line in file:
        line_processed = line.strip()
        id = line_processed.split(" ")[0]
        res[int(id) - 1].append(line_processed)
    file.close()

    return res


def validateAnswer(ins, answ):
    config = " "
    if ins.max_price:
        config += " ".join(ins.max_nodes)
    else: 
        config += ('0 ' * ins.n)[:-1]
    res_str = str(ins.id)+" "+str(ins.n)+" "+str(ins.max_price)+config
    return (res_str not in answ, res_str, answ)


def runProblems(paths):
    for path in paths:
        print("PATH: "+path)
        # answers = getAnswers(path)
        file = open(path, "r")
        RESULTS_TIME = []
        for line in file:
            ins = parseFile(line[:-1], path)

            ins.prepTable()
            start_time = time.time()
            ins.solveKnapsack_DP()
            end_time = time.time()

            if PRINT:
                ins.iprint()
            
            # test_res = validateAnswer(ins, answers[ins.id-1])
            time_res = end_time-start_time
            RESULTS_TIME.append(time_res)

            # if test_res[0]:
            #     print("TEST: " +str(ins.id)+" - FAIL: ", path+'\n')
            #     print("OUT: "+test_res[1]+"\nIS NOT AN ELEMENT OF\n"+"REF:", test_res[2])
            #     return   
            # else:
            if PRINT:
                print("ANSW: " +str(ins.id)+" - PASSED (elapsed time: "+"{:.10f}".format(time_res)+' sec.)')

        print() 
        print() 
        print(f'MAX_time: {"{:.10f}".format(max(RESULTS_TIME))} sec.') 
        print(f'AVG_time: { "{:.10f}".format( sum(RESULTS_TIME) / len(RESULTS_TIME) ) } sec.') 
        print("\n-------------------------------------------------\n")
        file.close()


if __name__ == "__main__":

    folder = "TST"
    paths = getFilesInFolder(folder)
    runProblems(paths)
