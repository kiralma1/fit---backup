# INSPIRED BY https://moodle-vyuka.cvut.cz/mod/page/view.php?id=153040 and https://moodle-vyuka.cvut.cz/mod/page/view.php?id=153039
import glob
from sys import excepthook
import time



instance_recursion_count = 0
INSTANCE_LENGTH = 500
INSTANCE_LENGTH_ZKW = 2000
MAX_INT = float("inf")
PRINT = 0
RESULTS_TIME = []
RESULTS_TIME = []
RESULTS_ERR = []
EPSILON = 0.3

class Instance:
    def __init__(self, id, n, M):
        self.id = int(id)
        self.n = int(n)  # number of items
        self.M = int(M)  # backpack capacity
        self.prices = []  # items - prices
        self.old_prices = []  # items - prices
        self.weights = []  # items - weight
        self.table = []
        self.max_price = -1
        self.scale = -1
        self.max_nodes = ['0' for x in range(self.n)]

        self.total_prices = 0

    def iprint(self):
        print("=================================================")
        print("ID:", self.id)
        print("n:", self.n)
        print("M:", self.M)
        print("scale:", self.scale)
        print("weights: {"+str(self.weights)[1:-1]+"}")
        print("prices: {"+str(self.prices)[1:-1]+"}")
        print("old_prices: {"+str(self.old_prices)[1:-1]+"}")
        print()
        print("max:", self.max_price)
        print(f'used items: {self.max_nodes}')
        print("--------------------------------")

    def table_print(self):
        for i, line in enumerate(reversed(self.table)):
            print(f'{"{:3}".format(len(self.table) - 1 - i)}|', end="")
            for cell in line:
                print(" ", end="")
                print(cell, end=" ")
            print()

        for i in range(self.n + 2):
            print(f'---', end="-")
        print()
        print("  #|", end=" ")
        for i in range(self.n + 1):
            print(f' {i} ', end="|")
        print()

    def buildSolution(self):
        for i, line in enumerate(reversed(self.table)):
            if line[-1] <= self.M:
                self.max_price = self.total_prices - i
                break

        tmp_max = self.max_price
        self.max_price = 0
        for i in range(self.n, 0, -1):
            if tmp_max < 0:
                tmp_max = 0

            if self.table[tmp_max][i] != self.table[tmp_max][i - 1]:
                tmp_max -= self.prices[i - 1]
                self.max_nodes[i - 1] = '1'
                self.max_price += self.old_prices[i - 1]

    def scaleItems(self):
        self.old_prices = [x for x in self.prices]
        max_price_valid = -1

        for item in range(self.n):
            if (self.prices[item] > max_price_valid and self.weights[item] <= self.M):
                max_price_valid = self.prices[item]
        self.scale = (max_price_valid * EPSILON) / self.n if EPSILON > 0 and max_price_valid > 0 else 1
        for i in range(self.n):
            self.prices[i] = int(self.prices[i] // self.scale)

    def prepTable(self):
        self.total_prices = sum(self.prices)
        self.table = [[0 for _ in range(self.n + 1)] for row in range(self.total_prices + 1)]
        
        for line in self.table:
            line[0] = MAX_INT
        try: 
            self.table[0][0] = 0
        except:
            self.iprint()
            print("self.scale:", self.scale)
            max_price_valid = -1
            for item in range(self.n):
                if (self.old_prices[item] > max_price_valid and self.weights[item] <= self.M):
                    max_price_valid = self.old_prices[item]
            print("max_price_valid:",max_price_valid)

            self.table_print()


            exit("ERR")

    def solveKnapsack_FPTAS(self):     
        # self.prepItems() 

        # if self.scaleItems() == "EMPTY_PRICES":
        #     return

        for row in range(1, self.n+1):
            for col in range(0, self.total_prices + 1):
                if col >= self.prices[row - 1]:
                    self.table[col][row] = min(
                        self.table[col][row - 1],
                        self.table[col - self.prices[row - 1]][row - 1] + self.weights[row - 1]
                    )
                else:
                    self.table[col][row] = self.table[col][row - 1]

        return self.buildSolution()
        

def getFilesInFolder(folder):
    return glob.glob('./data/'+folder+'/*_inst.dat')


def parseFile(file, path):
    file_split = file.split(" ")
    ins = Instance(file_split[0], file_split[1],
                   file_split[2])
    file_split = file_split[3:]
    for i in range(0, len(file_split), 2):
        ins.weights.append(int(file_split[i]))
        ins.prices.append(int(file_split[i+1]))  

    return ins


def getAnswers(path):
    p = path.split("/")
    p[-1] = p[-1].replace("inst", "sol")
    path = '/'.join(p)
    file = open(path, "r")
    print(f'GETTING ANSWERS FROM: {path}')

    res = [[] for _ in range(INSTANCE_LENGTH)] if "ZKW" not in path else [[] for _ in range(INSTANCE_LENGTH_ZKW)]
    for line in file:
        line_processed = line.strip()
        id = line_processed.split(" ")[0]
        res[int(id) - 1].append(line_processed)
    file.close()

    return res


def validateAnswer(ins, answ):
    real = int(answ[0].split(" ")[2])
    if real == 0:
        return "DIVISION_ERR_0"
    return ((real - ins.max_price) / real) * 100


def runProblems(paths):
    for path in paths:
        print("PATH: "+path)
        answers = getAnswers(path)
        file = open(path, "r")
        RESULTS_TIME = []
        RESULTS_ERR = []
        for line in file:
            ins = parseFile(line[:-1], path)

            ins.scaleItems()
            ins.prepTable()
            start_time = time.time()
            ins.solveKnapsack_FPTAS()
            end_time = time.time()

            if PRINT:
                ins.iprint()
            
            test_res = validateAnswer(ins, answers[ins.id-1])
            time_res = end_time-start_time
            if test_res == "DIVISION_ERR_0" and ins.max_price == 0:
                RESULTS_ERR.append(0)
                RESULTS_TIME.append(time_res)
            elif test_res == "DIVISION_ERR_0" or test_res == 100:
                RESULTS_TIME.append(time_res)
            else:
                RESULTS_ERR.append(test_res)
                RESULTS_TIME.append(time_res)
            
            if PRINT:
                ins.iprint()
                print("TEST " +str(ins.id)+" - OPTIMAL SOLUTION DIFFERENCE: "+str(test_res) + " (found in " + "{:.10f}".format(time_res) + " sec.)\n")
        print() 
        print() 
      
        print(f'MAX_time: {"{:.10f}".format(max(RESULTS_TIME))} sec.')
        print(f'MAX_err: {round(max(RESULTS_ERR), 2)} %\n')

        
        print(f'AVG_time: { "{:.10f}".format( sum(RESULTS_TIME) / len(RESULTS_TIME) ) } sec.')
        print(f'AVG_err: {  round(sum(RESULTS_ERR) / len(RESULTS_ERR), 2)} %')
        print("\n-------------------------------------------------\n")
       
        file.close()


if __name__ == "__main__":
    for i in [0.3, 0.5, 0.8]:
            EPSILON = i
            print(f'EPSILON: {EPSILON}')
            print("====================\n")
            folder = "NK"
            paths = getFilesInFolder(folder)
            runProblems(paths)

            folder = "ZKC"  
            paths = getFilesInFolder(folder)
            runProblems(paths)

            folder = "ZKW"
            paths = getFilesInFolder(folder)
            runProblems(paths)
            
