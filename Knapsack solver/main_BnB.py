import glob
import copy
from os import pathsep
import time


instance_recursion_count = 0
INSTANCE_LENGTH = 800
INSTANCE_LENGTH_ZKW = 2000
PRINT = 0
DEBUG = 1
RESULTS_TIME = []

class Node:
    def __init__(self, price, weight, layer, prev_items, ub = -1):
        self.price = price
        self.weight = weight
        self.ub = ub
        self.layer = layer
        self.prev_items = prev_items

    def nprint(self):
        print(f'PRICE: {self.price}')
        print(f'WEIGHT: {self.weight}')
        print(f'UB: {self.ub}')
        print(f'LAYER: {self.layer}')
        print("--------------------------")
        

class Instance:
    def __init__(self, id, n, M):
        self.id = int(id)
        self.n = int(n)  # number of items
        self.M = int(M)  # backpack capacity
        self.prices = []  # items - prices
        self.weights = []  # items - weight
        self.original_p_w = []
        
        self.max_price = 0
        self.max_nodes = []
        self.nodes = []

    def iprint(self):
        print("=================================================")
        print("ID:", self.id)
        print("n:", self.n)
        print("M:", self.M)
        print("weights: {"+str(self.weights)[1:-1]+"}")
        print("prices: {"+str(self.prices)[1:-1]+"}")
        print("original_pw_values: {"+str(self.original_p_w)[1:-1]+"}")
        print()
        print("max:", self.max_price)
        print(f'used items: {self.max_nodes}')
        print("--------------------------------")

    def upper(self, node):
        if node.weight <= self.M:
            total_p = node.price
            total_w = node.weight
            for i in range(node.layer+1, self.n):
                if total_w + self.weights[i] > self.M:
                    total_p += (self.M - total_w) * (self.prices[i] / self.weights[i])
                    break
                total_w += self.weights[i]
                total_p += self.prices[i]
            return total_p
        else:
            return 0

    def is_in_list(self, x, lst):
        # print("\t",lst, end="")
        try: 
            lst.remove(x)
            # print(f'  -  {x}  -  1')
            return True
        except ValueError:
            # print(f'  -  {x}  -  0')
            return False
                  
    def solveKnapsack_BnB(self):
        start_node = Node(0, 0, -1, [])
        start_node.ub = self.upper(start_node)
        
        self.nodes.append(start_node)

        while(len(self.nodes)):
            tmp = self.nodes.pop(0)
            if (tmp.layer == self.n - 1):
                continue
            else:
                left = Node(
                    tmp.price + self.prices[tmp.layer + 1], 
                    tmp.weight + self.weights[tmp.layer + 1], 
                    tmp.layer + 1, 
                    tmp.prev_items + [(self.prices[tmp.layer + 1], self.weights[tmp.layer + 1])], 
                    tmp.ub)
                if left.weight <= self.M and left.price > self.max_price:
                    tmp_prev_items = [x for x in left.prev_items]
                    self.max_nodes = ['1' if self.is_in_list(x, tmp_prev_items) else '0' for x in self.original_p_w ]
                    
                    self.max_price = left.price

                if left.ub > self.max_price:
                    self.nodes.append(left)

                right = Node(tmp.price, tmp.weight, tmp.layer + 1, tmp.prev_items)
                right.ub = self.upper(right)
                if right.ub > self.max_price:
                    self.nodes.append(right)
        return 



def getFilesInFolder(folder):
    return glob.glob('./data/'+folder+'/*.txt')


def parseFile(file, path):
    file_split = file.split(" ")
    ins = Instance(file_split[0], file_split[1],
                   file_split[2])
    file_split = file_split[3:]
    for i in range(0, len(file_split), 2):
        ins.weights.append(int(file_split[i]))
        ins.prices.append(int(file_split[i+1]))  

    ins.original_p_w = list(zip(copy.deepcopy(ins.prices), copy.deepcopy(ins.weights)))
    return ins


def getAnswers(path):
    p = path.split("/")
    p[-1] = p[-1].replace("inst", "sol")
    path = '/'.join(p)
    file = open(path, "r")
    print(f'GETTING ANSWERS FROM: {path}')

    res = [[] for _ in range(INSTANCE_LENGTH)] if "ZKW" not in path else [[] for _ in range(INSTANCE_LENGTH_ZKW)]
    for line in file:
        line_processed = line.strip()
        id = line_processed.split(" ")[0]
        res[int(id) - 1].append(line_processed)
    file.close()

    return res


def validateAnswer(ins, answ):
    if ins.max_price:
        config = " ".join(ins.max_nodes)
    else: 
        config = ('0 ' * ins.n)[:-1]
    res_str = str(ins.id)+" "+str(ins.n)+" "+str(ins.max_price)+" "+config
    return (res_str not in answ, res_str, answ)


def tidyTheInstance(ins, arr):
    for i, elem in enumerate(arr):
        ins.prices[i] = elem[0]
        ins.weights[i] = elem[1]


def runProblems(paths):
    for path in paths:
        print("PATH: "+path)
        if DEBUG:
            answ_f = open("./data/ANSWERS/RES_" + path.split("/")[-1], "w")
        file = open(path, "r")
        RESULTS_TIME = []
        for line in file:
            ins = parseFile(line[:-1], path)
            tmp_prices_weights = list(zip(ins.prices, ins.weights))
            tmp_prices_weights = sorted(tmp_prices_weights, key=lambda x: x[0]/x[1], reverse=True)
            tidyTheInstance(ins, tmp_prices_weights)
            
            start_time = time.time()
            ins.solveKnapsack_BnB()
            end_time = time.time()
            
            if PRINT:
                ins.iprint()
            
            # test_res = validateAnswer(ins, answers[ins.id-1])
            time_res = end_time - start_time
            RESULTS_TIME.append(time_res)

            # if test_res[0]:
            #     print("TEST: " +str(ins.id)+" - FAIL: ", path+'\n')
            #     print("OUT: "+test_res[1]+"\nIS NOT AN ELEMENT OF\n"+"REF:", test_res[2])
            #     return   
            # else:
            if PRINT:
                print("ANSW: " +str(ins.id)+" - PASSED (elapsed time: "+"{:.10f}".format(time_res)+' sec.)')
            
                if DEBUG:
                    print(f'{ins.id} {ins.M} {ins.max_price} {" ".join(ins.max_nodes)}')
            else:
                if DEBUG:
                    print(f'{ins.id} {ins.M} {ins.max_price} {" ".join(ins.max_nodes)}', file=answ_f)
        if DEBUG:
            answ_f.close()
            

        print() 
        print() 
        print(f'MAX_time: {"{:.10f}".format(max(RESULTS_TIME))} sec.') 
        print(f'AVG_time: { "{:.10f}".format( sum(RESULTS_TIME) / len(RESULTS_TIME) ) } sec.') 
        print("\n-------------------------------------------------\n")
        file.close()


if __name__ == "__main__":
    folder = "TST"
    paths = getFilesInFolder(folder)
    runProblems(paths)