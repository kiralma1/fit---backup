# INSPIRED BY https://www.geeksforgeeks.org/0-1-knapsack-problem-dp-10/
import glob
import copy
import time

INSTANCE_LENGTH = 500
INSTANCE_LENGTH_ZKW = 2000
PRINT = 0
RESULTS_TIME = []

class Instance:
    def __init__(self, id, n, M):
        self.id = int(id)
        self.n = int(n)  # number of items
        self.M = int(M)  # backpack capacity
        self.prices = []  # items - prices
        self.weights = []  # items - weight
        self.max_price = -1
        
        self.max_price = 0
        self.max_nodes = []

    def iprint(self):
        print("=================================================")
        print("ID:", self.id)
        print("n:", self.n)
        print("M:", self.M)
        print("weights: {"+str(self.weights)[1:-1]+"}")
        print("prices: {"+str(self.prices)[1:-1]+"}")
        print()
        print("max:", self.max_price)
        print(f'used items: {self.max_nodes}')
        print("--------------------------------")

    def solveKnapsack_BF(self, curr_M, curr_item):
        global instance_recursion_count

        # exit conditions - reaching leaf in rec tree or overloading the backpack
        if curr_item == 0 or curr_M <= 0:
            return 0
        
        # if current item would fit, chceck if value is bigger when item included or excluded
        if self.weights[curr_item - 1] <= curr_M:
                return max(
                    self.solveKnapsack_BF(curr_M - self.weights[curr_item - 1], curr_item-1) + self.prices[curr_item - 1], # include item
                    self.solveKnapsack_BF(curr_M, curr_item - 1) # exclude item
                )
        # if current item won't fit, exclude the item by default and continue in loading the backpack
        else:
            tmp = self.solveKnapsack_BF(curr_M, curr_item - 1)
            return tmp
            

def getFilesInFolder(folder):
    return glob.glob('./data/'+folder+'/*_inst.dat')


def parseFile(file, path):
    file_split = file.split(" ")
    ins = Instance(file_split[0], file_split[1],
                   file_split[2])
    file_split = file_split[3:]
    for i in range(0, len(file_split), 2):
        ins.weights.append(int(file_split[i]))
        ins.prices.append(int(file_split[i+1]))  
    return ins


def getAnswers(path):
    p = path.split("/")
    p[-1] = p[-1].replace("inst", "sol")
    path = '/'.join(p)
    file = open(path, "r")
    print(f'GETTING ANSWERS FROM: {path}')

    res = [[] for _ in range(INSTANCE_LENGTH)] if "ZKW" not in path else [[] for _ in range(INSTANCE_LENGTH_ZKW)]
    for line in file:
        line_processed = line.strip()
        id = line_processed.split(" ")[0]
        res[int(id) - 1].append(line_processed)
    file.close()

    return res


def validateAnswer(ins, answ):
    ins.max_nodes = answ[0].split(" ")[3:]

    if ins.max_price:
        config = " ".join(ins.max_nodes)
    else: 
        config = ('0 ' * ins.n)[:-1]
    res_str = str(ins.id)+" "+str(ins.n)+" "+str(ins.max_price)+" "+config
    return (res_str not in answ, res_str, answ)


def runProblems(paths):
    for path in paths:
        print("PATH: "+path)
        answers = getAnswers(path)
        file = open(path, "r")
        RESULTS_TIME = []

        for line in file:
            ins = parseFile(line[:-1], path)

            start_time = time.time()
            ins.max_price = ins.solveKnapsack_BF(ins.M, ins.n)
            end_time = time.time()
            
            if PRINT:
                ins.iprint()
            
            test_res = validateAnswer(ins, answers[ins.id-1])
            time_res = end_time - start_time
            RESULTS_TIME.append(time_res)

            if test_res[0]:
                print("TEST: " +str(ins.id)+" - FAIL: ", path+'\n')
                print("OUT: "+test_res[1]+"\nIS NOT AN ELEMENT OF\n"+"REF:", test_res[2])
                return   
            else:
                if PRINT:
                    print("ANSW: " +str(ins.id)+" - PASSED (elapsed time: "+"{:.10f}".format(time_res)+' sec.)')
                

        print() 
        print() 
        print(f'MAX_time: {"{:.10f}".format(max(RESULTS_TIME))} sec.') 
        print(f'AVG_time: { "{:.10f}".format( sum(RESULTS_TIME) / len(RESULTS_TIME) ) } sec.') 
        print("\n-------------------------------------------------\n")
        file.close()


if __name__ == "__main__":

    folder = "NK"
    paths = getFilesInFolder(folder)
    runProblems(paths)
    print("\n")

    folder = "ZR"
    paths = getFilesInFolder(folder)
    runProblems(paths)
