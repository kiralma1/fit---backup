# INSPIRED BY https://moodle-vyuka.cvut.cz/mod/page/view.php?id=153055
import glob
import copy
import time


instance_recursion_count = 0
INSTANCE_LENGTH = 800
INSTANCE_LENGTH_ZKW = 2000
PRINT = 1
RESULTS_TIME = []
RESULTS_ERR = []
        

class Instance:
    def __init__(self, id, n, M):
        self.id = int(id)
        self.n = int(n)  # number of items
        self.M = int(M)  # backpack capacity
        self.prices = []  # items - prices
        self.weights = []  # items - weight

        self.original_p_w = []
        
        self.max_price = 0
        self.max_nodes = []
        self.nodes = []

    def iprint(self):
        print("=================================================")
        print("ID:", self.id)
        print("n:", self.n)
        print("M:", self.M)
        print("weights: {"+str(self.weights)[1:-1]+"}")
        print("prices: {"+str(self.prices)[1:-1]+"}")
        print("original_pw_values: {"+str(self.original_p_w)[1:-1]+"}")
        print()
        print("max:", self.max_price)
        print(f'used items: {self.max_nodes}')
        print("--------------------------------")

    def is_in_list(self, x, lst):
        # print("\t",lst, end="")
        try: 
            lst.remove(x)
            # print(f'  -  {x}  -  1')
            return True
        except ValueError:
            # print(f'  -  {x}  -  0')
            return False
                  
    def solveKnapsack_greedy(self):
        global instance_recursion_count
        self.max_nodes = ['0' for x in self.prices]

        tmp_M = self.M
        i = 0
        for i in range(len(self.prices)):
            if self.weights[i] <= tmp_M:
                tmp_M -= self.weights[i]
                
                original_index = self.original_p_w.index((self.prices[i], self.weights[i]))
                original_index = original_index if self.prices[original_index] != '1' else self.original_p_w.index((self.prices[i], self.weights[i]), original_index + 1)

                self.max_nodes[original_index] = '1'
                self.max_price += self.prices[i]
            i += 1

        return 



def getFilesInFolder(folder):
    return glob.glob('./data/'+folder+'/*.txt')


def parseFile(file, path):
    file_split = file.split(" ")
    ins = Instance(file_split[0], file_split[1],
                   file_split[2])
    file_split = file_split[3:]
    for i in range(0, len(file_split), 2):
        ins.weights.append(int(file_split[i]))
        ins.prices.append(int(file_split[i+1]))  

    ins.original_p_w = list(zip(copy.deepcopy(ins.prices), copy.deepcopy(ins.weights)))
    return ins


def getAnswers(path):
    p = path.split("/")
    p[-1] = "RES_" + p[-1]
    p[-2] = "ANSWERS"
    path = '/'.join(p)
    file = open(path, "r")
    print(f'GETTING ANSWERS FROM: {path}')

    res = [[] for _ in range(INSTANCE_LENGTH)] if "ZKW" not in path else [[] for _ in range(INSTANCE_LENGTH_ZKW)]
    for line in file:
        line_processed = line.strip()
        id = line_processed.split(" ")[0]
        res[int(id) - 1].append(line_processed)
    file.close()

    return res


def validateAnswer(ins, answ):
    real = int(answ[0].split(" ")[2])
    if real == 0:
        return "DIVISION_ERR_0"
    return ((real - ins.max_price) / real) * 100 


def tidyTheInstance(ins, arr):
    for i, elem in enumerate(arr):
        ins.prices[i] = elem[0]
        ins.weights[i] = elem[1]


def runProblems(paths):
    global instance_recursion_count
    for path in paths:
        print("PATH: "+path)
        answers = getAnswers(path)
        file = open(path, "r")
        RESULTS_TIME = []
        RESULTS_ERR = []
        for line in file:
            instance_recursion_count = 0
            ins = parseFile(line[:-1], path)
            tmp_prices_weights = list(zip(ins.prices, ins.weights))
            tmp_prices_weights = sorted(tmp_prices_weights, key=lambda x: x[0]/x[1], reverse=True)
            tidyTheInstance(ins, tmp_prices_weights)
            start_time = time.time()
            ins.solveKnapsack_greedy()
            end_time = time.time()
            
            test_res = validateAnswer(ins, answers[ins.id-1])
            time_res = end_time-start_time
            if test_res == "DIVISION_ERR_0" and ins.max_price == 0:
                RESULTS_ERR.append(0)
                RESULTS_TIME.append(time_res)
            elif test_res != 100:
                RESULTS_ERR.append(test_res)
                RESULTS_TIME.append(time_res)
            
            if PRINT:
                ins.iprint()
                print("TEST " +str(ins.id)+" - OPTIMAL SOLUTION DIFFERENCE: "+str(test_res) + " (found in " + "{:.10f}".format(time_res) + " sec.)\n")
        print() 
        print() 

        print(f'AVG_time: { "{:.10f}".format( sum(RESULTS_TIME) / len(RESULTS_TIME) ) } sec.')
        print(f'MAX_time: {"{:.10f}".format(max(RESULTS_TIME))} sec.')

        print(f'AVG_err: {  round(sum(RESULTS_ERR) / len(RESULTS_ERR), 2)} %')
        print(f'MAX_err: {round(max(RESULTS_ERR), 2)} %\n')
        
        print("\n-------------------------------------------------\n")
       
        file.close()


if __name__ == "__main__":
    folder = "TST"
    paths = getFilesInFolder(folder)
    runProblems(paths)

